"""
Opdracht:

Bepalingen:
- Je moet gebruik maken van de aangeleverde variable(n)
- Je mag deze variable(n) niet aanpassen
- Het is de bedoeling dat je op het einde 1 programma hebt
- Inlever datum is zondag avond 13 maart 2022 18:00CET
- Als inlever formaat wordt een git url verwacht die gecloned kan worden
   
*  / 5 ptn 1 - Maak een public repository aan op jouw gitlab account voor dit project
*  /10 ptn 2 - Gebruik python om de gegeven api url aan te spreken
*  /20 ptn 3 - Gebruik regex om de volgende data te extracten:
*      - Jaar, maand en dag van de created date
*      - De provider van de nameservers (bijv voor 'ns3.combell.net' is de provider 'combell')
*  /15 ptn 4 - Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
*      - Het land van het domein
*      - Het ip van het domain
*      - De DNS provider van het domein
*      - Aparte jaar, maand en dag van de created date


Totaal  /50ptn
"""

""" voorbeeld yaml output

created:
  dag: '18'
  jaar: '2022'
  maand: '02'
ip: 185.162.31.124
land: BE
provider: combell

"""

url = "https://api.domainsdb.info/v1/domains/search?domain=syntra.be"

# Imports
import requests as api
import re
import yaml

# api communication
response = api.get(f"{url}")
response_data = response.json()

# regexes for each item
created = r"'create_date\':\s\'(?P<jaar>\d{4})-(?P<maand>\d{2})-(?P<dag>\d{2})"
land = r"'country':\s'(?P<land>\w{2})'"
ip = r"'A'\:\s\['(?P<ip>[0-9.]+)'\]"
provider = r"'NS'\:\s\['\w+.(?P<provider>\w+).\w+'"

# put here the items you want and in which order
regex_list = [created, ip, land, provider]

# logic for regexes and creating a list with all the needed info
output = []

for regex_pointer in regex_list:
  
  for match in re.finditer(regex_pointer, str(response_data)):
    
    if regex_pointer == created:
      output.append({"created": match.groupdict()})
      
    else:
      output.append(match.groupdict())

# print in yaml format
print(yaml.dump(output))